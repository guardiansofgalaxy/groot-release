﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FamillyTree.Models;

namespace FamillyTree.Controllers
{
    public class FamillyMemberController : Controller
    {
        private readonly TreeContext _context;

        public FamillyMemberController(TreeContext context)
        {
            _context = context;
        }

        // GET: FamillyMember
        public async Task<IActionResult> Index()
        {
            return View(await _context.FamillyMemberss.ToListAsync());
        }

        // GET: FamillyMember/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var famillyMembers = await _context.FamillyMemberss
                .FirstOrDefaultAsync(m => m.Id == id);
            if (famillyMembers == null)
            {
                return NotFound();
            }

            return View(famillyMembers);
        }

        // GET: FamillyMember/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: FamillyMember/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,SurName,Birthday,Photo,DiedDay,MaidenName,Note,Female")] FamillyMembers famillyMembers)
        {
            if (ModelState.IsValid)
            {
                _context.Add(famillyMembers);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(famillyMembers);
        }

        // GET: FamillyMember/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var famillyMembers = await _context.FamillyMemberss.FindAsync(id);
            if (famillyMembers == null)
            {
                return NotFound();
            }
            return View(famillyMembers);
        }

        // POST: FamillyMember/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,SurName,Birthday,Photo,DiedDay,MaidenName,Note,Female")] FamillyMembers famillyMembers)
        {
            if (id != famillyMembers.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(famillyMembers);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!FamillyMembersExists(famillyMembers.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(famillyMembers);
        }

        // GET: FamillyMember/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var famillyMembers = await _context.FamillyMemberss
                .FirstOrDefaultAsync(m => m.Id == id);
            if (famillyMembers == null)
            {
                return NotFound();
            }

            return View(famillyMembers);
        }

        // POST: FamillyMember/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var famillyMembers = await _context.FamillyMemberss.FindAsync(id);
            _context.FamillyMemberss.Remove(famillyMembers);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool FamillyMembersExists(int id)
        {
            return _context.FamillyMemberss.Any(e => e.Id == id);
        }
        
    }
}
