﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FamillyTree.Models
{
    public class FamillyMembers
    {

        [HiddenInput(DisplayValue = false)]

        public int Id { get; set; }

        [StringLength(50, MinimumLength = 3, ErrorMessage = "Длина строки должна быть от 3 до 50 символов")]
        [Required]
        [Display(Name = "Name")]
        public string Name { get; set; }

        [StringLength(50, MinimumLength = 3, ErrorMessage = "Длина строки должна быть от 3 до 50 символов")]
        [Required]
        [Display(Name = "Surname ")]
        public string SurName { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd'/'MM'/'yyyy}", ApplyFormatInEditMode = true)]
        [Required]
        [Display(Name = "Birthday")]
        public string Birthday { get; set; }
        // 
        [Required]
        [Display(Name = "Photo")]
        public string Photo { get; set; }

        [Required]
        [Display(Name = "Died Day")]
        public string DiedDay { get; set; }

        [StringLength(50, MinimumLength = 3, ErrorMessage = "Длина строки должна быть от 3 до 50 символов")]
        [Required]
        [Display(Name = "Maiden Name")]
        public string MaidenName { get; set; }

        [StringLength(50, MinimumLength = 3, ErrorMessage = "Длина строки должна быть от 3 до 50 символов")]
        [Required]
        [Display(Name = "Note")]
        public string Note { get; set; }

        [StringLength(50, MinimumLength = 3, ErrorMessage = "Длина строки должна быть от 3 до 50 символов")]
        [Required]
        [Display(Name = "Female")]
        public string Female { get; set; }
        
        [Required]
        [Display(Name = "Uses Name")]
        public int UserId { get; set; }

        public ICollection<User> User { get; set; }
    }
}
