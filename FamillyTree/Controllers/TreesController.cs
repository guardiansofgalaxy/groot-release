﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FamillyTree.Models;
using Microsoft.AspNetCore.Http;


namespace FamillyTree.Controllers
{
    public class TreesController : Controller
    {
        private readonly TreeContext _context;

        public TreesController(TreeContext context)
        {
            _context = context;
        }

        // GET: Trees
        public async Task<IActionResult> Index()
        {
            var timetable = from s in _context.Trees
                                //join c in _context.Chields on s.ChieldId equals c.ChieldId
                                //join p in _context.Parents on s.ParentsId equals p.ParentId
                            join f in _context.FamillyMemberss on s.MemberId equals f.Id
                            join u in _context.Users on s.UserId equals u.UserId
                            select new TreeViewModel()
                {
                    Tree = s,
                                User = u,
                                FamillyMember = f
                                //Chield = c,
                                //Parent = p
                            };

            timetable = timetable.Where(s => s.User.UserId.ToString().Equals(HttpContext.Session.GetString("UserId")));

            return View(await timetable.AsNoTracking().ToListAsync());
        }

        // GET: Trees/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tree = await _context.Trees
                .FirstOrDefaultAsync(m => m.TreePositionId == id);
            if (tree == null)
            {
                return NotFound();
            }

            return View(tree);
        }

        // GET: Trees/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Trees/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("TreePositionId,UserId,ParentsId,ChieldId,MemberId")] Tree tree)
        {
            tree.UserId = Int32.Parse(HttpContext.Session.GetString("UserId"));
            if (ModelState.IsValid)
            {
                _context.Add(tree);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(tree);
        }

        // GET: Trees/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tree = await _context.Trees.FindAsync(id);
            if (tree == null)
            {
                return NotFound();
            }
            return View(tree);
        }

        // POST: Trees/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("TreePositionId,UserId,ParentsId,ChieldId,MemberId")] Tree tree)
        {
            if (id != tree.TreePositionId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(tree);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TreeExists(tree.TreePositionId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(tree);
        }

        // GET: Trees/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tree = await _context.Trees
                .FirstOrDefaultAsync(m => m.TreePositionId == id);
            if (tree == null)
            {
                return NotFound();
            }

            return View(tree);
        }

        // POST: Trees/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var tree = await _context.Trees.FindAsync(id);
            _context.Trees.Remove(tree);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TreeExists(string id)
        {
            return _context.Trees.Any(e => e.TreePositionId == id);
        }
    }
}
