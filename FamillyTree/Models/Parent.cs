﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FamillyTree.Models
{
    public class Parent
    {
        [HiddenInput(DisplayValue = false)]
        public int ParentId { get; set; }

        [StringLength(50, MinimumLength = 3, ErrorMessage = "Длина строки должна быть от 3 до 50 символов")]
        [Required]
        [Display(Name = "Wife")]
        public string WifeId { get; set; }

        [StringLength(50, MinimumLength = 3, ErrorMessage = "Длина строки должна быть от 3 до 50 символов")]
        [Required]
        [Display(Name = "Husband")]
        public string HusbandId { get; set; }

    }
}
