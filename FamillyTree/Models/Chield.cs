﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FamillyTree.Models
{
    public class Chield
    {


        [HiddenInput(DisplayValue = false)]
   
        public int ChieldId { get; set; }

        [Required]
        [Display(Name = "Child1")]
        public string Chield1 { get; set; }

        [Required]
        [Display(Name = "Child2")]
        public string Chield2 { get; set; }

        [Required]
        [Display(Name = "Child3")]
        public string Chield3 { get; set; }

        [Required]
        [Display(Name = "Child4")]
        public string Chield4 { get; set; }

        [Required]
        [Display(Name = "Child5")]
        public string Chield5 { get; set; }
    }
}
