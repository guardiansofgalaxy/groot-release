﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FamillyTree.Models
{
    public class User
    {
        // ID 

        [HiddenInput(DisplayValue = false)]
        public int UserId { get; set; }

        [StringLength(50, MinimumLength = 3, ErrorMessage = "Lenght must be from 3 to 50 sumbols")]
        [Required]
        [Display(Name = "Name")]
        public string Name { get; set; }

        [StringLength(50, MinimumLength = 3, ErrorMessage = "Lenght must be from 3 to 50 sumbols")]
        [Required]
        [Display(Name = "SurName")]
        public string SurName { get; set; }

        [RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}", ErrorMessage = "incorect adress")]
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Password is required.")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Compare("Password", ErrorMessage = "Please confirm your Password.")]
        [DataType(DataType.Password)]
        public string ConfirmPassword { get; set; }

        public ProjectEnums.Roles Type { get; set; }

        public FamillyMembers FamillyMembers { get; set; }

    }
}
