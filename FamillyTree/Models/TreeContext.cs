﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace FamillyTree.Models
{
    

        public class TreeContext : DbContext
        {

        public TreeContext(DbContextOptions<TreeContext> options)
            : base(options)
        {
        }

        public DbSet<User> Users { get; set; }
            public DbSet<Parent> Parents { get; set; }
          public DbSet<FamillyMembers> FamillyMemberss { get; set; }
            public DbSet<Tree> Trees { get; set; }
            public DbSet<Chield> Chields { get; set; }
        }
    }

