﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;


namespace FamillyTree.Models
{
    public class Tree
    {
        [Key]
        public string TreePositionId { get; set; }

        public int UserId { get; set; }
        public int ParentsId { get; set; }
        public int ChieldId { get; set; }
        public int MemberId { get; set; }

        public ICollection<User> Users { get; set; }
        public ICollection<Chield> Chields { get; set; }
        public ICollection<Parent> Parents { get; set; }
      public ICollection<FamillyMembers> FamillyMemberss { get; set; }




    }



}

