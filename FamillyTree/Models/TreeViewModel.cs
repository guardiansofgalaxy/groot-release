﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FamillyTree.Models
{
    public class TreeViewModel
    {
        public Tree Tree { get; set; }
        public User User { get; set; }
        public FamillyMembers FamillyMember { get; set; }
        public  Chield Chield { get; set; }
        public  Parent Parent { get; set; }

    }
}
